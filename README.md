# Behance User Search

## Demonstration

![Alt Text](./image1.png)
![Alt Text](./image2.png)

## Getting Started:

1. clone this repo
2. type `npm install` in the terminal in the folder that contains this repo
3. type `npm run start`in this same folder
4. This will open up a tab in your broswer which will contain a search bar. Go to this tab
5. Type in the name in the search bar
6. Click on one of the options that opens. This will take you to a new page where you can see information about this user

## Testing:

In the repo for this folder, run `npm run cypress:open`. This will open a broswer in which you can see end-to-end tests. Click on `End-to-end.spec.js` and the test will run in the broswer. Verify that all the steps are successful.

## Info about project:

This project uses React Hooks and Material Design. According to new paradigms, this application doesn't follow the presentional/container component pattern, but instead everything is a functional component, making the code easier to read and debug

## TODO with more time:

- add more tests (functional and unit tests)
- implement JS doc for type checking and documentation
- show owner image rather than name for project owners
- make dates show how long ago rather than actual time using moment.js
- mock server responses for end-to-end test so test will pass even when the server is down
- make API key hidden

## Project Images
