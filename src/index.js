import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import NotFound from "./NotFound";
import Search from "./Search";
import UserPage from "./UserPage";

const routing = (
  <Router>
    <Switch>
      <Route path="/user/:username" component={UserPage} />
      <Route exact path="/" component={Search} />
      <Route exact path="/search" component={Search} />
      <Route component={NotFound} />
    </Switch>
  </Router>
);

ReactDOM.render(routing, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
