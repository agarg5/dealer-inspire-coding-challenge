import { DATE_FORMAT_OPTIONS } from "./constants";
import AutosuggestHighlightMatch from "autosuggest-highlight/match";

export const getFormattedDate = date =>
  date &&
  new Date(date * 1000).toLocaleDateString("en-us", DATE_FORMAT_OPTIONS);

export const getMatchPercentage = (searchResult, query) =>
  searchResult.length > 0 &&
  AutosuggestHighlightMatch(searchResult, query).reduce(
    (accumulator, currentValue) =>
      currentValue.length !== 2
        ? accumulator
        : accumulator + currentValue[1] - currentValue[0],
    0.0
  ) / searchResult.length;
