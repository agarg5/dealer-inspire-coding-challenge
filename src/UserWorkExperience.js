import "./UserPage.css";
import {
  API,
  USE_SAMPLE_DATA,
  SAMPLE_USER_WORK_EXPERIENCE,
  HTTP_SUCCESS
} from "./constants";
import { CLIENT_ID } from "./clientID";
import { ClipLoader } from "react-spinners";
import { get } from "lodash";
import {
  Typography,
  Card,
  CardContent,
  ExpansionPanel,
  ExpansionPanelDetails,
  ListItem,
  List,
  ExpansionPanelSummary
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PropTypes from "prop-types";
import QueryString from "query-string";
import React, { useState } from "react";

const WorkExperience = props => {
  const { position, organization, location } = props;
  return (
    <ListItem>
      <Card>
        <CardContent>
          {position && (
            <label>
              <Typography inline={true}>Position:</Typography>
              <Typography>{position}</Typography>
            </label>
          )}
          {organization && (
            <label>
              <Typography inline={true}>Organization:</Typography>
              <Typography>{organization}</Typography>
            </label>
          )}
          {location && (
            <label>
              <Typography inline={true}>Location:</Typography>
              <Typography>{location}</Typography>
            </label>
          )}
        </CardContent>
      </Card>
    </ListItem>
  );
};

function UserWorkExperience(props) {
  const username = get(props, "username");
  const [workExperience, setWorkExperience] = useState();
  const [errorMessage, setErrorMessage] = useState();

  const getDataFromNetwork = async () => {
    const params = {
      client_id: CLIENT_ID
    };
    if (USE_SAMPLE_DATA) {
      setWorkExperience(SAMPLE_USER_WORK_EXPERIENCE.work_experience);
      return;
    }
    console.log(
      `${API}users/${username}/work_experience?${QueryString.stringify(params)}`
    );
    fetch(
      `${API}users/${username}/work_experience?${QueryString.stringify(params)}`
    )
      .then((response, error) => {
        if (response.status === HTTP_SUCCESS) {
          return response;
        }
        throw Error(response.statusText);
      })
      .catch(error => setErrorMessage(error.message))
      .then(response => response && response.json())
      .then(result => {
        if (result) {
          setErrorMessage(null);
          setWorkExperience(get(result, "work_experience"));
        }
      });
  };

  const showWorkExperience = () => {
    if (errorMessage) return errorMessage;
    if (!workExperience) return <ClipLoader />;
    if (workExperience.length === 0) return "No Work Experience";
    return (
      <List>
        {workExperience.map(experience => {
          const { position, organization, location } = experience;
          return (
            <WorkExperience
              position={position}
              organization={organization}
              location={location}
              key={[position, organization, location].join()}
            />
          );
        })}
      </List>
    );
  };

  return (
    <ExpansionPanel
      onChange={(event, expanded) => {
        if (workExperience) return;
        if (expanded) getDataFromNetwork();
      }}
      className="Work-Experience"
    >
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className="heading">Work Experience</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{showWorkExperience()}</ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default UserWorkExperience;

UserWorkExperience.propTypes = {
  username: PropTypes.string.isRequired
};
