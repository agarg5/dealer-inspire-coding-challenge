import "./UserPage.css";
import { API, SAMPLE_USER_DATA, USE_SAMPLE_DATA } from "./constants";
import { CLIENT_ID } from "./clientID";
import { ClipLoader } from "react-spinners";
import { get } from "lodash";
import {
  Typography,
  ListItem,
  List,
  ListItemText,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import QueryString from "query-string";
import React, { useState, useEffect } from "react";
import ReactRouterPropTypes from "react-router-prop-types";
import ReadMoreReact from "read-more-react";
import UserFollowers from "./UserFollowers";
import UserFollowing from "./UserFollowing";
import UserProjects from "./UserProjects";
import UserStats from "./UserStats";
import UserWorkExperience from "./UserWorkExperience";
import UserBasicInfo from "./UserBasicInfo";

function UserPage(props) {
  const username = get(props, "match.params.username");
  const [user, setUserData] = useState(
    USE_SAMPLE_DATA && SAMPLE_USER_DATA.user
  );
  const getDataFromNetwork = async () => {
    const params = {
      client_id: CLIENT_ID
    };
    if (USE_SAMPLE_DATA) return;
    if (user) return;
    fetch(`${API}users/${username}?${QueryString.stringify(params)}`)
      .then(response => response.json())
      .then(result => setUserData(get(result, "user")));
  };

  useEffect(() => {
    getDataFromNetwork();
  }, [props.username]);

  if (!user) {
    return <ClipLoader loading={true} />;
  }

  const { links, sections } = user;

  const LinksDisplay = links && (
    <ExpansionPanel className="Links">
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className="heading">Links</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        {links.length === 0 ? (
          <Typography>No Links</Typography>
        ) : (
          <List>
            {links.map(link => {
              const { title, url } = link;
              return (
                <ListItem key={url}>
                  <ListItemText>
                    <a href={url}>{title}</a>
                  </ListItemText>
                </ListItem>
              );
            })}
          </List>
        )}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );

  const SectionsDisplay = sections && (
    <ExpansionPanel className="About">
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className="heading">About</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>
        {Object.entries(sections).length === 0 ? (
          <Typography>No Information to Show</Typography>
        ) : (
          <List>
            {Object.entries(sections).map(entry => {
              const [title, description] = entry;
              return (
                <ListItem key={title}>
                  <label>
                    <Typography variant="subtitle1">{title}</Typography>
                    <ReadMoreReact text={description} />
                  </label>
                </ListItem>
              );
            })}
          </List>
        )}
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );

  return (
    <>
      <div className="header">
        <UserBasicInfo user={user} main={true} />
      </div>
      {SectionsDisplay}
      {LinksDisplay}
      <UserProjects username={username} />
      <UserFollowers username={username} />
      <UserFollowing username={username} />
      <UserWorkExperience username={username} />
      <UserStats username={username} />
    </>
  );
}

export default UserPage;

UserPage.propTypes = {
  username: ReactRouterPropTypes.match
};
