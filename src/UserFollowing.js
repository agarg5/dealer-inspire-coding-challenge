import "./UserPage.css";
import {
  API,
  SAMPLE_USER_FOLLOWING,
  USE_SAMPLE_DATA,
  HTTP_SUCCESS
} from "./constants";
import { CLIENT_ID } from "./clientID";
import { ClipLoader } from "react-spinners";
import { get } from "lodash";
import {
  Typography,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PropTypes from "prop-types";
import QueryString from "query-string";
import React, { useState } from "react";
import UserCard from "./UserCard";

function UserFollowing(props) {
  const username = get(props, "username");
  const [following, setFollowing] = useState();
  const [errorMessage, setErrorMessage] = useState();

  const getDataFromNetwork = async () => {
    const params = {
      client_id: CLIENT_ID
    };
    if (USE_SAMPLE_DATA) {
      setFollowing(SAMPLE_USER_FOLLOWING.following);
      return;
    }
    console.log(
      `${API}users/${username}/following?${QueryString.stringify(params)}`
    );
    fetch(`${API}users/${username}/following?${QueryString.stringify(params)}`)
      .then(response => {
        if (response.status === HTTP_SUCCESS) {
          return response;
        }
        throw Error(response.statusText);
      })
      .catch(error => {
        setErrorMessage(error.message);
      })
      .then(response => response.json())
      .then(result => {
        if (result) {
          setErrorMessage(null);
          setFollowing(get(result, "following"));
        }
      });
  };

  const showFollowing = () => {
    if (errorMessage) return errorMessage;
    if (!following) return <ClipLoader />;
    if (following.length === 0) return "No Following";
    return (
      <div className="following-list">
        {following.map(follower => (
          <UserCard userInfo={follower} key={follower.username} />
        ))}
      </div>
    );
  };

  return (
    <ExpansionPanel
      onChange={(event, expanded) => {
        if (following) return;
        if (expanded) getDataFromNetwork();
      }}
      className="Following"
    >
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className="heading">Following</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{showFollowing()}</ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default UserFollowing;

UserFollowing.propTypes = {
  username: PropTypes.string.isRequired
};
