import "./UserPage.css";
import { getFormattedDate } from "./utils";
import { SocialIcon } from "react-social-icons";
import { Typography } from "@material-ui/core";
import PropTypes from "prop-types";
import React from "react";

const IMAGE_SIZE = 138;

function UserBasicInfo(props) {
  const { user, main } = props;

  if (!user) return null;

  const {
    fields,
    city,
    state,
    country,
    company,
    occupation,
    created_on: createdOnTimestampSeconds,
    url,
    twitter,
    display_name,
    images
  } = user;

  const fieldsDisplay = fields && <Typography>{fields.join(" | ")}</Typography>;

  const URLDisplay = url && (
    <Typography className={`url-display`}>
      <a href={url}>Behance Profile</a>
    </Typography>
  );

  const createdOnDisplay = (
    <Typography>
      {createdOnTimestampSeconds &&
        `Member since ${getFormattedDate(createdOnTimestampSeconds)}`}
    </Typography>
  );

  const occupationDisplay = (occupation || company) && (
    <>
      <Typography>{occupation && occupation}</Typography>
      <Typography>{company && company}</Typography>
    </>
  );

  const twitterDisplay = twitter && (
    <>
      <SocialIcon url={`https://twitter.com/${twitter}`} />
      <Typography>{twitter}</Typography>
    </>
  );

  const fromDisplay = (city || state || country) && (
    <>
      <Typography>
        {[city, state, country].filter(val => val.length > 0).join(", ")}
      </Typography>
    </>
  );

  const NameDisplay = display_name && (
    <Typography
      variant={main ? "h3" : "h5"}
      className="name-display"
      gutterBottom
    >
      {display_name}
    </Typography>
  );

  const imageDisplay = images && (
    <img src={images[IMAGE_SIZE]} alt={`${display_name || `user's image`}`} />
  );

  return (
    <>
      {imageDisplay}
      {NameDisplay}
      {occupationDisplay}
      {URLDisplay}
      {fromDisplay}
      {createdOnDisplay}
      {fieldsDisplay}
      {twitterDisplay}
    </>
  );
}

export default UserBasicInfo;

UserBasicInfo.propTypes = {
  user: PropTypes.object,
  main: PropTypes.bool
};
