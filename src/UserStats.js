import "./UserPage.css";
import {
  API,
  SAMPLE_USER_STATS,
  USE_SAMPLE_DATA,
  HTTP_SUCCESS
} from "./constants";
import { CLIENT_ID } from "./clientID";
import { ClipLoader } from "react-spinners";
import { get } from "lodash";
import {
  Typography,
  Card,
  CardContent,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PropTypes from "prop-types";
import QueryString from "query-string";
import React, { useState } from "react";

const Stat = props => {
  const {
    profile_views,
    project_appreciations,
    project_comments,
    project_views
  } = props.info;

  return (
    <CardContent>
      {project_views >= 0 && (
        <Typography>Project Views: {project_views}</Typography>
      )}
      <br />
      {project_appreciations >= 0 && (
        <label>
          <Typography inline={true}>Project Appreciations: </Typography>
          <Typography inline={true}>{project_appreciations}</Typography>
        </label>
      )}
      <br />
      {project_comments >= 0 && (
        <label>
          <Typography inline={true}>Project Comments: </Typography>
          <Typography inline={true}>{project_comments}</Typography>
        </label>
      )}
      <br />
      {profile_views >= 0 && (
        <label>
          <Typography inline={true}>Profile Views: </Typography>
          <Typography inline={true}>{profile_views}</Typography>
        </label>
      )}
    </CardContent>
  );
};

function UserStats(props) {
  const username = get(props, "username");
  const [stats, setStats] = useState();
  const [errorMessage, setErrorMessage] = useState();

  const getDataFromNetwork = async () => {
    const params = {
      client_id: CLIENT_ID
    };
    if (USE_SAMPLE_DATA) {
      setStats(SAMPLE_USER_STATS.stats);
      return;
    }
    console.log(
      `${API}users/${username}/stats?${QueryString.stringify(params)}`
    );
    fetch(`${API}users/${username}/stats?${QueryString.stringify(params)}`)
      .then(response => {
        if (response.status === HTTP_SUCCESS) {
          return response;
        }
        throw Error(response.statusText);
      })
      .catch(error => setErrorMessage(error.message))
      .then(response => response && response.json())
      .then(result => {
        if (result) {
          setErrorMessage(null);
          setStats(get(result, "stats"));
        }
      });
  };

  const showStats = () => {
    if (errorMessage) return errorMessage;
    if (!stats) return <ClipLoader />;
    const { today, all_time: allTime } = stats;
    if (!allTime && !today) return "No Stats";
    return (
      <div className="stat-list">
        <Card className="stat-card">
          {today && (
            <div className="card-header">
              <Typography variant="subtitle1">Today</Typography>
              <Stat info={today} />
            </div>
          )}
        </Card>
        <Card className="stat-card">
          {allTime && (
            <div className="card-header">
              <Typography variant="subtitle1">All Time</Typography>
              <Stat info={allTime} />
            </div>
          )}
        </Card>
      </div>
    );
  };

  return (
    <ExpansionPanel
      onChange={(event, expanded) => {
        if (stats) return;
        if (expanded) getDataFromNetwork();
      }}
      className="Stats"
    >
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className="heading">Stats</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{showStats()}</ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default UserStats;

UserStats.propTypes = {
  username: PropTypes.string.isRequired
};
