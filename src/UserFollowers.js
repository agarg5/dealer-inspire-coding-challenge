import "./UserPage.css";
import {
  API,
  SAMPLE_USER_FOLLOWERS,
  USE_SAMPLE_DATA,
  HTTP_SUCCESS
} from "./constants";
import { CLIENT_ID } from "./clientID";
import { ClipLoader } from "react-spinners";
import { get } from "lodash";
import {
  Typography,
  ExpansionPanel,
  ExpansionPanelDetails,
  ExpansionPanelSummary
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import PropTypes from "prop-types";
import QueryString from "query-string";
import React, { useState } from "react";
import UserCard from "./UserCard";

function UserFollowers(props) {
  const username = get(props, "username");
  const [followers, setFollowers] = useState();
  const [errorMessage, setErrorMessage] = useState();

  const getDataFromNetwork = async () => {
    const params = {
      client_id: CLIENT_ID
    };
    if (USE_SAMPLE_DATA) {
      setFollowers(SAMPLE_USER_FOLLOWERS.followers);
      return;
    }
    console.log(
      `${API}users/${username}/followers?${QueryString.stringify(params)}`
    );
    fetch(`${API}users/${username}/followers?${QueryString.stringify(params)}`)
      .then(response => {
        if (response.status === HTTP_SUCCESS) {
          return response;
        }
        throw Error(response.statusText);
      })
      .catch(error => {
        setErrorMessage(error.message);
      })
      .then(response => response.json())
      .then(result => {
        if (result) {
          setErrorMessage(null);
          setFollowers(get(result, "followers"));
        }
      });
  };

  const showFollowers = () => {
    if (errorMessage) return errorMessage;
    if (!followers) return <ClipLoader />;
    if (followers.length === 0) return "No Followers";
    return (
      <div className="follower-list">
        {followers.map(follower => (
          <UserCard userInfo={follower} key={follower.username} />
        ))}
      </div>
    );
  };

  return (
    <ExpansionPanel
      onChange={(event, expanded) => {
        if (followers) return;
        if (expanded) getDataFromNetwork();
      }}
      className="Followers"
    >
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className="heading">Followers</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{showFollowers()}</ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default UserFollowers;

UserFollowers.propTypes = {
  username: PropTypes.string.isRequired
};
