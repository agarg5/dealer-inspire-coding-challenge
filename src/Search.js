import "./Search.css";
import {
  API,
  SAMPLE_USERS_DATA,
  USE_SAMPLE_DATA,
  SAMPLE_USER_IMAGE
} from "./constants";
import { getMatchPercentage } from "./utils";
import { CLIENT_ID } from "./clientID";
import { ClipLoader } from "react-spinners";
import { get } from "lodash";
import { Typography } from "@material-ui/core";
import Autosuggest from "react-autosuggest";
import QueryString from "query-string";
import React, { useState, useEffect } from "react";
import UserSearchResult from "./UserSearchResult";

const IMAGE_SIZE = 50;

/* 
  number of miliseconds after input changes in search bar to wait for further changes before hitting server
  prevents overburdening network connection with too many requests from fast typing
*/
const BOUNCE = 75;

function Search(props) {
  const [query, setQuery] = useState("");
  const [searchBoxValue, setSearchBoxValue] = useState("");
  const [users, setUsers] = useState(
    USE_SAMPLE_DATA ? SAMPLE_USERS_DATA.users : []
  );
  const [loading, setLoading] = useState(false);

  const getUsersFromNetwork = async (searchedQuery, queryResultsNum) => {
    setLoading(true);
    const params = {
      client_id: CLIENT_ID,
      q: query
    };
    fetch(`${API}users?${QueryString.stringify(params)}`)
      .then(response => {
        setLoading(false);
        if (response.ok) {
          return response;
        }
        throw Error(response.statusText);
      })
      .then(response => response.json())
      .then(result => {
        if (searchedQuery === query) {
          let newUsers = get(result, "users");
          console.log(newUsers);
          if (newUsers)
            setUsers(
              newUsers.sort(
                (a, b) =>
                  getMatchPercentage(b.display_name, query) -
                  getMatchPercentage(a.display_name, query)
              )
            );
        }
      });
  };

  /*
    hit server and update users whenever the query changes
  */
  useEffect(() => {
    if (!USE_SAMPLE_DATA) getUsersFromNetwork(query);
  }, [query]);

  useEffect(() => {
    window.setTimeout(setQuery(searchBoxValue), BOUNCE);
  }, [searchBoxValue]);

  return (
    <>
      <br />
      <Typography variant="h2" align="center" gutterBottom>
        Search For a User
      </Typography>
      <div className="search-bar">
        <Autosuggest
          suggestions={users}
          onSuggestionsClearRequested={() => setUsers([])}
          onSuggestionsFetchRequested={input => {}}
          onSuggestionSelected={(event, suggestionInfo) => {
            const username = get(suggestionInfo, "suggestion.username");
            props.history.push(`/user/${username}`);
          }}
          getSuggestionValue={user => user.display_name}
          renderSuggestion={user => (
            <div className={user.username}>
              <UserSearchResult
                query={searchBoxValue}
                suggestionText={user.display_name}
                imageLink={
                  USE_SAMPLE_DATA
                    ? SAMPLE_USER_IMAGE
                    : get(user, `images.${IMAGE_SIZE}`)
                }
              />
            </div>
          )}
          inputProps={{
            placeholder: "Enter the name of a user here",
            value: searchBoxValue,
            onChange: event => setSearchBoxValue(event.target.value)
          }}
        />
        <br />
        <ClipLoader loading={loading} />
      </div>
    </>
  );
}

export default Search;
