import "./UserPage.css";
import { Card } from "@material-ui/core/";
import UserBasicInfo from "./UserBasicInfo";
import React from "react";

const UserCard = props =>
  props.userInfo && (
    <Card className="user-card">
      <UserBasicInfo user={props.userInfo} />
    </Card>
  );

export default UserCard;
