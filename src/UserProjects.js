import {
  API,
  SAMPLE_USER_PROJECTS,
  USE_SAMPLE_DATA,
  HTTP_SUCCESS
} from "./constants";
import Project from "./Project";
import { CLIENT_ID } from "./clientID";
import { ClipLoader } from "react-spinners";
import { get } from "lodash";
import { Typography } from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import PropTypes from "prop-types";
import QueryString from "query-string";
import React, { useState } from "react";

function UserProjects(props) {
  const username = get(props, "username");
  const [projects, setProjects] = useState();
  const [errorMessage, setErrorMessage] = useState();

  const getDataFromNetwork = async () => {
    const params = {
      client_id: CLIENT_ID
    };
    if (USE_SAMPLE_DATA) {
      setProjects(SAMPLE_USER_PROJECTS.projects);
      return;
    }
    console.log(
      `${API}users/${username}/projects?${QueryString.stringify(params)}`
    );
    fetch(`${API}users/${username}/projects?${QueryString.stringify(params)}`)
      .then(response => response.json())
      .then(response => {
        if (response.http_code === HTTP_SUCCESS) {
          return response;
        }
        throw Error(response.statusText);
      })
      .catch(error => {
        setErrorMessage(error.message);
      })
      .then(result => {
        if (result) {
          setErrorMessage(null);
          setProjects(get(result, "projects"));
        }
      });
  };

  const showProjects = () => {
    if (errorMessage) return errorMessage;
    if (!projects) return <ClipLoader />;
    if (projects.length === 0) return "No Projects";
    return (
      <div className="project-list">
        {projects.map(project => (
          <Project project={project} key={project.id} />
        ))}
      </div>
    );
  };

  return (
    <ExpansionPanel
      onChange={(event, expanded) => {
        if (projects) return;
        if (expanded) getDataFromNetwork();
      }}
      className="Projects"
    >
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography className="heading">Projects</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails>{showProjects()}</ExpansionPanelDetails>
    </ExpansionPanel>
  );
}

export default UserProjects;

UserProjects.propTypes = {
  username: PropTypes.string.isRequired
};
