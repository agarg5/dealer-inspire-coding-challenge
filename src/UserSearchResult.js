import "./Search.css";
import AutosuggestHighlightMatch from "autosuggest-highlight/match";
import AutosuggestHighlightParse from "autosuggest-highlight/parse";
import PropTypes from "prop-types";
import React from "react";

function UserSearchResult(props) {
  const { query, suggestionText, imageLink } = props;
  const matches = AutosuggestHighlightMatch(suggestionText, query);
  const parts = AutosuggestHighlightParse(suggestionText, matches);

  return (
    <span className="suggestion-content">
      <img src={imageLink} alt={``} />

      <span className="name">
        {parts.map((part, index) => {
          const className = part.highlight ? "highlight" : null;

          return (
            <span className={className} key={index}>
              {part.text}
            </span>
          );
        })}
      </span>
    </span>
  );
}

export default UserSearchResult;

UserSearchResult.propTypes = {
  query: PropTypes.string.isRequired,
  suggestionText: PropTypes.string.isRequired,
  imageLink: PropTypes.string
};
