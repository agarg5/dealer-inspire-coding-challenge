const CORS_ANYWHERE_PROXY = "https://cors-anywhere.herokuapp.com";
export const API = `${CORS_ANYWHERE_PROXY}/http://behance.net/v2/`;

export const USE_SAMPLE_DATA = false;

export const HTTP_SUCCESS = 200;

export const DATE_FORMAT_OPTIONS = {
  year: "numeric",
  day: "numeric",
  month: "short"
};

export const SAMPLE_QUERY = "Matias Corea";

export const SAMPLE_USER_IMAGE =
  "https://mir-s3-cdn-cf.behance.net/user/50/a240a13344259.54fdfb1da91e1.png";

export const SAMPLE_USER_DATA = {
  user: {
    id: 50001,
    first_name: "Matias",
    last_name: "Corea",
    username: "MatiasCorea",
    city: "Brooklyn",
    state: "New York",
    country: "United States",
    company: "Behance",
    occupation: "Chief Designer & Co-Founder",
    created_on: 1182475806,
    url: "http://www.behance.net/MatiasCorea",
    display_name: "Matias Corea",
    images: {
      "32":
        "http://behance.vo.llnwd.net/profiles/50001/32xac8d5163265f6898d0b970dbfcdf4868.png",
      "50":
        "http://behance.vo.llnwd.net/profiles/50001/50xac8d5163265f6898d0b970dbfcdf4868.png",
      "78":
        "http://behance.vo.llnwd.net/profiles/50001/78xac8d5163265f6898d0b970dbfcdf4868.png",
      "115":
        "http://behance.vo.llnwd.net/profiles/50001/115xac8d5163265f6898d0b970dbfcdf4868.png",
      "129":
        "http://behance.vo.llnwd.net/profiles/50001/129xac8d5163265f6898d0b970dbfcdf4868.png",
      "138":
        "http://behance.vo.llnwd.net/profiles/50001/ac8d5163265f6898d0b970dbfcdf4868.png"
    },
    fields: ["Web Design", "Typography", "Interaction Design"],
    twitter: "@matiascorea",
    links: [
      {
        title: "My Blog",
        url: "http://www.matiascorea.com/blog"
      },
      {
        title: "My Twitter Feed",
        url: "www.twitter.com/matiascorea"
      },
      {
        title: "Desing Droplets Interview",
        url:
          "http://designdroplets.com/designer-qa/matias-corea-chief-designer-behance/"
      },
      {
        title: "Inclined To create Mag Interview",
        url:
          "http://inclinedtocreate.com/blog/2010/09/09/interview-matias-corea/"
      },
      {
        title: "My Project Interaction Talk",
        url: "http://projectinteraction.org/category/students/"
      },
      {
        title: "SwissMiss On the 99% Grpahics",
        url:
          "http://www.swiss-miss.com/2010/05/99-conference-2010-motion-graphics.html"
      }
    ],
    sections: {
      "Where, When and What":
        "I spent my school years freelancing out of my father's architecture studio, so architecture and urbanism have had a huge influence on the way I approach design. From form to function.\n\nFollowing school in La Massana, I spent two years working for various small design shops in Barcelona like Zeligmedia, M.A.Y.A Design, and Triada Communication Agency. Eventually I felt that I needed to leave Barcelona to look for other opportunities, and I found myself heading to NYC in October 2002.\n\nI was lucky to be hired by Michael Ian Kaye (former Creative Director at Oglivy) to work at AR Media, where I had the chance to work on accounts such as Versace, Dolce & Gabanna, Valentino, Ann Taylor, Naturalizer, Influence Magazine, Escada, and many more.\n\nAfter that, I worked for Pro Am under the strict vigilance of John Malcolmson and Simon Endres, where I learned the craft of branding, and a love for design process.\n\nNow I find myself as Chief Designer at Behance, trying to make the lives of other creatives better."
    }
  }
};

export const SAMPLE_USERS_DATA = {
  users: [
    {
      id: 193936,
      first_name: "Matias",
      last_name: "Chilo",
      username: "vanth",
      city: "Buenos Aires",
      state: "",
      country: "Argentina",
      company: "",
      occupation: "",
      created_on: 1279082706,
      url: "http://www.behance.net/vanth",
      display_name: "Matias Chilo",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles4/193936/32x4463cd4adf137aa218b15657eaa568b1.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles4/193936/50x4463cd4adf137aa218b15657eaa568b1.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles4/193936/78x4463cd4adf137aa218b15657eaa568b1.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles4/193936/115x4463cd4adf137aa218b15657eaa568b1.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles4/193936/129x4463cd4adf137aa218b15657eaa568b1.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles4/193936/4463cd4adf137aa218b15657eaa568b1.jpg"
      },
      fields: ["Design", "Editorial Design", "Graphic Design"]
    },
    {
      id: 50001,
      first_name: "Matias",
      last_name: "Corea",
      username: "MatiasCorea",
      city: "Brooklyn",
      state: "New York",
      country: "United States",
      company: "Behance",
      occupation: "Chief Designer & Co-Founder",
      created_on: 1182475806,
      url: "http://www.behance.net/MatiasCorea",
      display_name: "Matias Corea",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles/50001/32xac8d5163265f6898d0b970dbfcdf4868.png",
        "50":
          "http://behance.vo.llnwd.net/profiles/50001/50xac8d5163265f6898d0b970dbfcdf4868.png",
        "78":
          "http://behance.vo.llnwd.net/profiles/50001/78xac8d5163265f6898d0b970dbfcdf4868.png",
        "115":
          "http://behance.vo.llnwd.net/profiles/50001/115xac8d5163265f6898d0b970dbfcdf4868.png",
        "129":
          "http://behance.vo.llnwd.net/profiles/50001/129xac8d5163265f6898d0b970dbfcdf4868.png",
        "138":
          "http://behance.vo.llnwd.net/profiles/50001/ac8d5163265f6898d0b970dbfcdf4868.png"
      },
      fields: ["Web Design", "Typography", "Interaction Design"]
    },
    {
      id: 963453,
      first_name: "Matias",
      last_name: "Furno",
      username: "matiasfurno",
      city: "Buenos Aires",
      state: "",
      country: "Argentina",
      company: "",
      occupation: "Motion Designer",
      created_on: 1330232439,
      url: "http://www.behance.net/matiasfurno",
      display_name: "Matias Furno",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles17/963453/32xdd99b78e4a524e3dc7f1acd5a319cd0e.png",
        "50":
          "http://behance.vo.llnwd.net/profiles17/963453/50xdd99b78e4a524e3dc7f1acd5a319cd0e.png",
        "78":
          "http://behance.vo.llnwd.net/profiles17/963453/78xdd99b78e4a524e3dc7f1acd5a319cd0e.png",
        "115":
          "http://behance.vo.llnwd.net/profiles17/963453/115xdd99b78e4a524e3dc7f1acd5a319cd0e.png",
        "129":
          "http://behance.vo.llnwd.net/profiles17/963453/129xdd99b78e4a524e3dc7f1acd5a319cd0e.png",
        "138":
          "http://behance.vo.llnwd.net/profiles17/963453/dd99b78e4a524e3dc7f1acd5a319cd0e.png"
      },
      fields: ["Animation", "Digital Art", "Graphic Design"]
    },
    {
      id: 119502,
      first_name: "Matias",
      last_name: "Klingsholm",
      username: "klingsholm",
      city: "Shanghai",
      state: "",
      country: "China",
      company: "Student",
      occupation: "Graphic designer",
      created_on: 1250532885,
      url: "http://www.behance.net/klingsholm",
      display_name: "Matias Klingsholm",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles4/119502/32x766fe4bd281e02b9be0ce33e0713bd26.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles4/119502/50x766fe4bd281e02b9be0ce33e0713bd26.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles4/119502/78x766fe4bd281e02b9be0ce33e0713bd26.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles4/119502/115x766fe4bd281e02b9be0ce33e0713bd26.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles4/119502/129x766fe4bd281e02b9be0ce33e0713bd26.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles4/119502/766fe4bd281e02b9be0ce33e0713bd26.jpg"
      },
      fields: ["Branding", "Illustration", "Graphic Design"]
    },
    {
      id: 107325,
      first_name: "Julian Matias",
      last_name: "Guridi",
      username: "jmguridi",
      city: "Buenos Aires",
      state: "",
      country: "Argentina",
      company: "Guridi",
      occupation: "",
      created_on: 1244033059,
      url: "http://www.behance.net/jmguridi",
      display_name: "Julian Matias Guridi",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles3/107325/32x01073251244310571.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles3/107325/50x01073251244310571.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles3/107325/78x01073251244310571.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles3/107325/115x01073251244310571.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles3/107325/129x01073251244310571.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles3/107325/01073251244310571.jpg"
      },
      fields: ["Graphic Design", "Multimedia", "Web Design"]
    },
    {
      id: 1518275,
      first_name: "AndrÃÂ©",
      last_name: "Matias",
      username: "andrematias",
      city: "Espinho",
      state: "",
      country: "Portugal",
      company: "",
      occupation: "Photographer",
      created_on: 1345943737,
      url: "http://www.behance.net/andrematias",
      display_name: "AndrÃÂ© Matias",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles8/1518275/32x125f2a33b49487ca18977e64880d541e.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles8/1518275/50x125f2a33b49487ca18977e64880d541e.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles8/1518275/78x125f2a33b49487ca18977e64880d541e.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles8/1518275/115x125f2a33b49487ca18977e64880d541e.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles8/1518275/129x125f2a33b49487ca18977e64880d541e.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles8/1518275/125f2a33b49487ca18977e64880d541e.jpg"
      },
      fields: ["Photography", "Photojournalism", "Digital Art"]
    },
    {
      id: 813651,
      first_name: "MatÃÂ­as",
      last_name: "Scappaticcio",
      username: "scapa",
      city: "Buenos Aires",
      state: "",
      country: "Argentina",
      company: "",
      occupation: "Graphic Designer",
      created_on: 1324349787,
      url: "http://www.behance.net/scapa",
      display_name: "MatÃÂ­as Scappaticcio",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles20/813651/32xcf95f1685d1fe37a3e05589be9d6de88.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles20/813651/50xcf95f1685d1fe37a3e05589be9d6de88.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles20/813651/78xcf95f1685d1fe37a3e05589be9d6de88.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles20/813651/115xcf95f1685d1fe37a3e05589be9d6de88.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles20/813651/129xcf95f1685d1fe37a3e05589be9d6de88.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles20/813651/cf95f1685d1fe37a3e05589be9d6de88.jpg"
      },
      fields: ["Graphic Design", "Illustration", "Typography"]
    },
    {
      id: 222307,
      first_name: "Matias",
      last_name: "Conti",
      username: "matiasconti",
      city: "Barcelona",
      state: "",
      country: "Spain",
      company: "MatiasConti Design",
      occupation: "",
      created_on: 1282902242,
      url: "http://www.behance.net/matiasconti",
      display_name: "Matias Conti",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles4/222307/32xeeec70ec27dc4f92184ecb8d6afad3d6.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles4/222307/50xeeec70ec27dc4f92184ecb8d6afad3d6.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles4/222307/78xeeec70ec27dc4f92184ecb8d6afad3d6.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles4/222307/115xeeec70ec27dc4f92184ecb8d6afad3d6.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles4/222307/129xeeec70ec27dc4f92184ecb8d6afad3d6.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles4/222307/eeec70ec27dc4f92184ecb8d6afad3d6.jpg"
      },
      fields: ["Toy Design", "Product Design", "Industrial Design"]
    },
    {
      id: 1514863,
      first_name: "Matias",
      last_name: "Cisnero",
      username: "cosasdenegro",
      city: "Buenos Aires",
      state: "",
      country: "Argentina",
      company: "",
      occupation: "DiseÃÂ±o Industrial",
      created_on: 1345835171,
      url: "http://www.behance.net/cosasdenegro",
      display_name: "Matias Cisnero",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles8/1514863/32xf4abb420e8e512d697c9243a47d3d908.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles8/1514863/50xf4abb420e8e512d697c9243a47d3d908.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles8/1514863/78xf4abb420e8e512d697c9243a47d3d908.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles8/1514863/115xf4abb420e8e512d697c9243a47d3d908.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles8/1514863/129xf4abb420e8e512d697c9243a47d3d908.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles8/1514863/f4abb420e8e512d697c9243a47d3d908.jpg"
      },
      fields: ["Industrial Design"]
    },
    {
      id: 1516137,
      first_name: "Heron",
      last_name: "Matias",
      username: "heron_matias",
      city: "Rio de Janeiro",
      state: "",
      country: "Brazil",
      company: "",
      occupation: "Designer grÃÂ¡fico e ilustrador",
      created_on: 1345872147,
      url: "http://www.behance.net/heron_matias",
      display_name: "Heron Matias",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles26/1516137/32x7669a39096c8b12162481bfcb21213c8.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles26/1516137/50x7669a39096c8b12162481bfcb21213c8.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles26/1516137/78x7669a39096c8b12162481bfcb21213c8.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles26/1516137/115x7669a39096c8b12162481bfcb21213c8.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles26/1516137/129x7669a39096c8b12162481bfcb21213c8.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles26/1516137/7669a39096c8b12162481bfcb21213c8.jpg"
      },
      fields: ["Digital Art", "Graphic Design"]
    },
    {
      id: 409595,
      first_name: "Matia",
      last_name: "Gobbo",
      username: "matiagobbo",
      city: "London",
      state: "",
      country: "United Kingdom",
      company: "Matia Gobbo",
      occupation: "Graphic designer",
      created_on: 1302770731,
      url: "http://www.behance.net/matiagobbo",
      display_name: "Matia Gobbo",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles15/409595/32x4ecac6201f92f93180c867789470344b.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles15/409595/50x4ecac6201f92f93180c867789470344b.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles15/409595/78x4ecac6201f92f93180c867789470344b.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles15/409595/115x4ecac6201f92f93180c867789470344b.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles15/409595/129x4ecac6201f92f93180c867789470344b.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles15/409595/4ecac6201f92f93180c867789470344b.jpg"
      },
      fields: ["Graphic Design", "Web Design", "Branding"]
    },
    {
      id: 58672,
      first_name: "Matias",
      last_name: "Jansen",
      username: "Matias",
      city: "City",
      state: "",
      country: "Netherlands",
      company: "",
      occupation: "",
      created_on: 1202913954,
      url: "http://www.behance.net/Matias",
      display_name: "Matias Jansen",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles/58672/32x0c7ec7bb9706a0d8692bc9c481a4dc40.png",
        "50":
          "http://behance.vo.llnwd.net/profiles/58672/50x0c7ec7bb9706a0d8692bc9c481a4dc40.png",
        "78":
          "http://behance.vo.llnwd.net/profiles/58672/78x0c7ec7bb9706a0d8692bc9c481a4dc40.png",
        "115":
          "http://behance.vo.llnwd.net/profiles/58672/115x0c7ec7bb9706a0d8692bc9c481a4dc40.png",
        "129":
          "http://behance.vo.llnwd.net/profiles/58672/129x0c7ec7bb9706a0d8692bc9c481a4dc40.png",
        "138":
          "http://behance.vo.llnwd.net/profiles/58672/0c7ec7bb9706a0d8692bc9c481a4dc40.png"
      },
      fields: ["Web Design", "Graphic Design"]
    }
  ]
};

export const SAMPLE_USER_FOLLOWERS = {
  followers: [
    {
      id: 495335,
      first_name: "Florian",
      last_name: "Schulz",
      username: "getflourish",
      city: "Berlin",
      state: "",
      country: "Germany",
      company: "",
      occupation: "Interaction Design Student",
      created_on: 1307960997,
      url: "http://www.behance.net/getflourish",
      display_name: "Florian Schulz",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles22/495335/32x6fa4740750791e0e5e6b5b38a9be7641.png",
        "50":
          "http://behance.vo.llnwd.net/profiles22/495335/50x6fa4740750791e0e5e6b5b38a9be7641.png",
        "78":
          "http://behance.vo.llnwd.net/profiles22/495335/78x6fa4740750791e0e5e6b5b38a9be7641.png",
        "115":
          "http://behance.vo.llnwd.net/profiles22/495335/115x6fa4740750791e0e5e6b5b38a9be7641.png",
        "129":
          "http://behance.vo.llnwd.net/profiles22/495335/129x6fa4740750791e0e5e6b5b38a9be7641.png",
        "138":
          "http://behance.vo.llnwd.net/profiles22/495335/6fa4740750791e0e5e6b5b38a9be7641.png"
      },
      fields: ["User Interface Design", "Interaction Design", "Motion Graphics"]
    },
    {
      id: 1757145,
      first_name: "Amy",
      last_name: "Moore",
      username: "AmyMoorePhotography",
      city: "Brooklyn",
      state: "New York",
      country: "United States",
      company: "",
      occupation: "First Assistant",
      created_on: 1351220521,
      url: "http://www.behance.net/AmyMoorePhotography",
      display_name: "Amy Moore",
      images: {
        "32": "http://assets.behance.net/img/profile/no-image-32.jpg",
        "50": "http://assets.behance.net/img/profile/no-image-50.jpg",
        "78": "http://assets.behance.net/img/profile/no-image-78.jpg",
        "115": "http://assets.behance.net/img/profile/no-image-138.jpg",
        "129": "http://assets.behance.net/img/profile/no-image-138.jpg",
        "138": "http://assets.behance.net/img/profile/no-image-138.jpg"
      },
      fields: ["Creative Direction", "Photography", "Retouching"]
    },
    {
      id: 145571,
      first_name: "Maxime",
      last_name: "DUBREUCQ",
      username: "daxime",
      city: "San Francisco",
      state: "California",
      country: "United States",
      company: "LUNAR",
      occupation: "Industrial Designer",
      created_on: 1264341363,
      url: "http://www.behance.net/daxime",
      display_name: "Maxime DUBREUCQ",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles3/145571/32x71d250c31adf3b59164c612a2a7b1e73.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles3/145571/50x71d250c31adf3b59164c612a2a7b1e73.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles3/145571/78x71d250c31adf3b59164c612a2a7b1e73.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles3/145571/115x71d250c31adf3b59164c612a2a7b1e73.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles3/145571/129x71d250c31adf3b59164c612a2a7b1e73.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles3/145571/71d250c31adf3b59164c612a2a7b1e73.jpg"
      },
      fields: ["Editorial Design", "Industrial Design", "Product Design"]
    },
    {
      id: 1254153,
      first_name: "Sean",
      last_name: "Blanda",
      username: "Blanda",
      city: "Brooklyn",
      state: "New York",
      country: "United States",
      company: "Behance/99U",
      occupation: "Associate Editor",
      created_on: 1339872868,
      url: "http://www.behance.net/Blanda",
      display_name: "Sean Blanda",
      images: {
        "32": "http://assets.behance.net/img/profile/no-image-32.jpg",
        "50": "http://assets.behance.net/img/profile/no-image-50.jpg",
        "78": "http://assets.behance.net/img/profile/no-image-78.jpg",
        "115": "http://assets.behance.net/img/profile/no-image-138.jpg",
        "129": "http://assets.behance.net/img/profile/no-image-138.jpg",
        "138": "http://assets.behance.net/img/profile/no-image-138.jpg"
      },
      fields: ["Journalism", "Writing"]
    }
  ]
};

export const SAMPLE_USER_PROJECTS = {
  projects: [
    {
      id: 3882857,
      name: "The ALVA Award",
      published_on: 1338993558,
      created_on: 1336591701,
      modified_on: 1338993615,
      url: "http://www.behance.net/gallery/The-ALVA-Award/3882857",
      fields: ["Branding", "Product Design"],
      covers: {
        "115":
          "http://behance.vo.llnwd.net/profiles/50001/projects/3882857/115xb09a269d4b56d6ff5c640364208d3480.jpg",
        "202":
          "http://behance.vo.llnwd.net/profiles/50001/projects/3882857/b09a269d4b56d6ff5c640364208d3480.jpg"
      },
      mature_content: 0,
      owners: {
        "50001": {
          id: 50001,
          first_name: "Matias",
          last_name: "Corea",
          username: "MatiasCorea",
          city: "Brooklyn",
          state: "New York",
          country: "United States",
          company: "Behance",
          occupation: "Chief Designer & Co-Founder",
          created_on: 1182475806,
          url: "http://www.behance.net/MatiasCorea",
          display_name: "Matias Corea",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles/50001/32xac8d5163265f6898d0b970dbfcdf4868.png",
            "50":
              "http://behance.vo.llnwd.net/profiles/50001/50xac8d5163265f6898d0b970dbfcdf4868.png",
            "78":
              "http://behance.vo.llnwd.net/profiles/50001/78xac8d5163265f6898d0b970dbfcdf4868.png",
            "115":
              "http://behance.vo.llnwd.net/profiles/50001/115xac8d5163265f6898d0b970dbfcdf4868.png",
            "129":
              "http://behance.vo.llnwd.net/profiles/50001/129xac8d5163265f6898d0b970dbfcdf4868.png",
            "138":
              "http://behance.vo.llnwd.net/profiles/50001/ac8d5163265f6898d0b970dbfcdf4868.png"
          },
          fields: ["Web Design", "Typography", "Interaction Design"]
        },
        "216667": {
          id: 216667,
          first_name: "Jocelyn K.",
          last_name: "Glei",
          username: "jkglei",
          city: "New York",
          state: "New York",
          country: "United States",
          company: "99U",
          occupation: "Director & Editor-in-Chief",
          created_on: 1281999212,
          url: "http://www.behance.net/jkglei",
          display_name: "Jocelyn K. Glei",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles5/216667/32xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "50":
              "http://behance.vo.llnwd.net/profiles5/216667/50xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "78":
              "http://behance.vo.llnwd.net/profiles5/216667/78xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "115":
              "http://behance.vo.llnwd.net/profiles5/216667/115xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "129":
              "http://behance.vo.llnwd.net/profiles5/216667/129xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "138":
              "http://behance.vo.llnwd.net/profiles5/216667/fa36ffb549a1fa9d4a16a0f8d1709146.jpg"
          },
          fields: ["Editing", "Writing", "Publishing"]
        }
      },
      stats: {
        views: 1870,
        appreciations: 148,
        comments: 15
      }
    },
    {
      id: 3856261,
      name: "99% Conference 2012: Identity & Branded Materials",
      published_on: 1336512105,
      created_on: 1336400037,
      modified_on: 1345057073,
      url:
        "http://www.behance.net/gallery/99-Conference-2012-Identity-Branded-Materials/3856261",
      fields: ["Branding", "Graphic Design", "Print Design"],
      covers: {
        "115":
          "http://behance.vo.llnwd.net/profiles10/371633/projects/3856261/115x9896ba327b0136b1d67230fc31277a3f.jpg",
        "202":
          "http://behance.vo.llnwd.net/profiles10/371633/projects/3856261/9896ba327b0136b1d67230fc31277a3f.jpg"
      },
      mature_content: 0,
      owners: {
        "371633": {
          id: 371633,
          first_name: "Raewyn",
          last_name: "Brandon",
          username: "raewynbrandon",
          city: "New York",
          state: "New York",
          country: "United States",
          company: "",
          occupation: "Web & Graphic Designer",
          created_on: 1300570825,
          url: "http://www.behance.net/raewynbrandon",
          display_name: "Raewyn Brandon",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles10/371633/32x4cadd7e2b4340429ffc29736fcc417bc.jpg",
            "50":
              "http://behance.vo.llnwd.net/profiles10/371633/50x4cadd7e2b4340429ffc29736fcc417bc.jpg",
            "78":
              "http://behance.vo.llnwd.net/profiles10/371633/78x4cadd7e2b4340429ffc29736fcc417bc.jpg",
            "115":
              "http://behance.vo.llnwd.net/profiles10/371633/115x4cadd7e2b4340429ffc29736fcc417bc.jpg",
            "129":
              "http://behance.vo.llnwd.net/profiles10/371633/129x4cadd7e2b4340429ffc29736fcc417bc.jpg",
            "138":
              "http://behance.vo.llnwd.net/profiles10/371633/4cadd7e2b4340429ffc29736fcc417bc.jpg"
          },
          fields: ["Graphic Design", "Print Design", "Web Design"]
        },
        "216667": {
          id: 216667,
          first_name: "Jocelyn K.",
          last_name: "Glei",
          username: "jkglei",
          city: "New York",
          state: "New York",
          country: "United States",
          company: "99U",
          occupation: "Director & Editor-in-Chief",
          created_on: 1281999212,
          url: "http://www.behance.net/jkglei",
          display_name: "Jocelyn K. Glei",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles5/216667/32xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "50":
              "http://behance.vo.llnwd.net/profiles5/216667/50xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "78":
              "http://behance.vo.llnwd.net/profiles5/216667/78xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "115":
              "http://behance.vo.llnwd.net/profiles5/216667/115xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "129":
              "http://behance.vo.llnwd.net/profiles5/216667/129xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "138":
              "http://behance.vo.llnwd.net/profiles5/216667/fa36ffb549a1fa9d4a16a0f8d1709146.jpg"
          },
          fields: ["Editing", "Writing", "Publishing"]
        },
        "50001": {
          id: 50001,
          first_name: "Matias",
          last_name: "Corea",
          username: "MatiasCorea",
          city: "Brooklyn",
          state: "New York",
          country: "United States",
          company: "Behance",
          occupation: "Chief Designer & Co-Founder",
          created_on: 1182475806,
          url: "http://www.behance.net/MatiasCorea",
          display_name: "Matias Corea",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles/50001/32xac8d5163265f6898d0b970dbfcdf4868.png",
            "50":
              "http://behance.vo.llnwd.net/profiles/50001/50xac8d5163265f6898d0b970dbfcdf4868.png",
            "78":
              "http://behance.vo.llnwd.net/profiles/50001/78xac8d5163265f6898d0b970dbfcdf4868.png",
            "115":
              "http://behance.vo.llnwd.net/profiles/50001/115xac8d5163265f6898d0b970dbfcdf4868.png",
            "129":
              "http://behance.vo.llnwd.net/profiles/50001/129xac8d5163265f6898d0b970dbfcdf4868.png",
            "138":
              "http://behance.vo.llnwd.net/profiles/50001/ac8d5163265f6898d0b970dbfcdf4868.png"
          },
          fields: ["Web Design", "Typography", "Interaction Design"]
        },
        "511814": {
          id: 511814,
          first_name: "Behance",
          last_name: "",
          username: "behance",
          city: "New York",
          state: "New York",
          country: "United States",
          company: "Behance",
          occupation: "",
          created_on: 1308938660,
          url: "http://www.behance.net/behance",
          display_name: "Behance",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles25/511814/32xe68d9ad064e2df842a010331a04f253f.jpg",
            "50":
              "http://behance.vo.llnwd.net/profiles25/511814/50xe68d9ad064e2df842a010331a04f253f.jpg",
            "78":
              "http://behance.vo.llnwd.net/profiles25/511814/78xe68d9ad064e2df842a010331a04f253f.jpg",
            "115":
              "http://behance.vo.llnwd.net/profiles25/511814/115xe68d9ad064e2df842a010331a04f253f.jpg",
            "129":
              "http://behance.vo.llnwd.net/profiles25/511814/129xe68d9ad064e2df842a010331a04f253f.jpg",
            "138":
              "http://behance.vo.llnwd.net/profiles25/511814/e68d9ad064e2df842a010331a04f253f.jpg"
          },
          fields: ["Design", "Interaction Design", "Web Development"]
        }
      },
      stats: {
        views: 24186,
        appreciations: 1495,
        comments: 113
      }
    },
    {
      id: 3872017,
      name: "99% Conference Materials 2011",
      published_on: 1337205965,
      created_on: 1336510012,
      modified_on: 1339095842,
      url:
        "http://www.behance.net/gallery/99-Conference-Materials-2011/3872017",
      fields: ["Branding", "Graphic Design", "Print Design"],
      covers: {
        "115":
          "http://behance.vo.llnwd.net/profiles/50001/projects/3872017/115x31f14060eadcd1dddf2a821702444576.jpg",
        "202":
          "http://behance.vo.llnwd.net/profiles/50001/projects/3872017/31f14060eadcd1dddf2a821702444576.jpg"
      },
      mature_content: 0,
      owners: {
        "50001": {
          id: 50001,
          first_name: "Matias",
          last_name: "Corea",
          username: "MatiasCorea",
          city: "Brooklyn",
          state: "New York",
          country: "United States",
          company: "Behance",
          occupation: "Chief Designer & Co-Founder",
          created_on: 1182475806,
          url: "http://www.behance.net/MatiasCorea",
          display_name: "Matias Corea",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles/50001/32xac8d5163265f6898d0b970dbfcdf4868.png",
            "50":
              "http://behance.vo.llnwd.net/profiles/50001/50xac8d5163265f6898d0b970dbfcdf4868.png",
            "78":
              "http://behance.vo.llnwd.net/profiles/50001/78xac8d5163265f6898d0b970dbfcdf4868.png",
            "115":
              "http://behance.vo.llnwd.net/profiles/50001/115xac8d5163265f6898d0b970dbfcdf4868.png",
            "129":
              "http://behance.vo.llnwd.net/profiles/50001/129xac8d5163265f6898d0b970dbfcdf4868.png",
            "138":
              "http://behance.vo.llnwd.net/profiles/50001/ac8d5163265f6898d0b970dbfcdf4868.png"
          },
          fields: ["Web Design", "Typography", "Interaction Design"]
        },
        "216667": {
          id: 216667,
          first_name: "Jocelyn K.",
          last_name: "Glei",
          username: "jkglei",
          city: "New York",
          state: "New York",
          country: "United States",
          company: "99U",
          occupation: "Director & Editor-in-Chief",
          created_on: 1281999212,
          url: "http://www.behance.net/jkglei",
          display_name: "Jocelyn K. Glei",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles5/216667/32xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "50":
              "http://behance.vo.llnwd.net/profiles5/216667/50xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "78":
              "http://behance.vo.llnwd.net/profiles5/216667/78xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "115":
              "http://behance.vo.llnwd.net/profiles5/216667/115xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "129":
              "http://behance.vo.llnwd.net/profiles5/216667/129xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "138":
              "http://behance.vo.llnwd.net/profiles5/216667/fa36ffb549a1fa9d4a16a0f8d1709146.jpg"
          },
          fields: ["Editing", "Writing", "Publishing"]
        },
        "511814": {
          id: 511814,
          first_name: "Behance",
          last_name: "",
          username: "behance",
          city: "New York",
          state: "New York",
          country: "United States",
          company: "Behance",
          occupation: "",
          created_on: 1308938660,
          url: "http://www.behance.net/behance",
          display_name: "Behance",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles25/511814/32xe68d9ad064e2df842a010331a04f253f.jpg",
            "50":
              "http://behance.vo.llnwd.net/profiles25/511814/50xe68d9ad064e2df842a010331a04f253f.jpg",
            "78":
              "http://behance.vo.llnwd.net/profiles25/511814/78xe68d9ad064e2df842a010331a04f253f.jpg",
            "115":
              "http://behance.vo.llnwd.net/profiles25/511814/115xe68d9ad064e2df842a010331a04f253f.jpg",
            "129":
              "http://behance.vo.llnwd.net/profiles25/511814/129xe68d9ad064e2df842a010331a04f253f.jpg",
            "138":
              "http://behance.vo.llnwd.net/profiles25/511814/e68d9ad064e2df842a010331a04f253f.jpg"
          },
          fields: ["Design", "Interaction Design", "Web Development"]
        }
      },
      stats: {
        views: 11545,
        appreciations: 623,
        comments: 50
      }
    },
    {
      id: 3871843,
      name: "99% Magazine 2011",
      published_on: 1337203255,
      created_on: 1336508969,
      modified_on: 1338142485,
      url: "http://www.behance.net/gallery/99-Magazine-2011/3871843",
      fields: ["Editorial Design", "Graphic Design", "Print Design"],
      covers: {
        "115":
          "http://behance.vo.llnwd.net/profiles/50001/projects/3871843/115xfe68102be885ff02913314f29a522615.jpg",
        "202":
          "http://behance.vo.llnwd.net/profiles/50001/projects/3871843/fe68102be885ff02913314f29a522615.jpg"
      },
      mature_content: 0,
      owners: {
        "50001": {
          id: 50001,
          first_name: "Matias",
          last_name: "Corea",
          username: "MatiasCorea",
          city: "Brooklyn",
          state: "New York",
          country: "United States",
          company: "Behance",
          occupation: "Chief Designer & Co-Founder",
          created_on: 1182475806,
          url: "http://www.behance.net/MatiasCorea",
          display_name: "Matias Corea",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles/50001/32xac8d5163265f6898d0b970dbfcdf4868.png",
            "50":
              "http://behance.vo.llnwd.net/profiles/50001/50xac8d5163265f6898d0b970dbfcdf4868.png",
            "78":
              "http://behance.vo.llnwd.net/profiles/50001/78xac8d5163265f6898d0b970dbfcdf4868.png",
            "115":
              "http://behance.vo.llnwd.net/profiles/50001/115xac8d5163265f6898d0b970dbfcdf4868.png",
            "129":
              "http://behance.vo.llnwd.net/profiles/50001/129xac8d5163265f6898d0b970dbfcdf4868.png",
            "138":
              "http://behance.vo.llnwd.net/profiles/50001/ac8d5163265f6898d0b970dbfcdf4868.png"
          },
          fields: ["Web Design", "Typography", "Interaction Design"]
        },
        "216667": {
          id: 216667,
          first_name: "Jocelyn K.",
          last_name: "Glei",
          username: "jkglei",
          city: "New York",
          state: "New York",
          country: "United States",
          company: "99U",
          occupation: "Director & Editor-in-Chief",
          created_on: 1281999212,
          url: "http://www.behance.net/jkglei",
          display_name: "Jocelyn K. Glei",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles5/216667/32xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "50":
              "http://behance.vo.llnwd.net/profiles5/216667/50xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "78":
              "http://behance.vo.llnwd.net/profiles5/216667/78xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "115":
              "http://behance.vo.llnwd.net/profiles5/216667/115xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "129":
              "http://behance.vo.llnwd.net/profiles5/216667/129xfa36ffb549a1fa9d4a16a0f8d1709146.jpg",
            "138":
              "http://behance.vo.llnwd.net/profiles5/216667/fa36ffb549a1fa9d4a16a0f8d1709146.jpg"
          },
          fields: ["Editing", "Writing", "Publishing"]
        },
        "511814": {
          id: 511814,
          first_name: "Behance",
          last_name: "",
          username: "behance",
          city: "New York",
          state: "New York",
          country: "United States",
          company: "Behance",
          occupation: "",
          created_on: 1308938660,
          url: "http://www.behance.net/behance",
          display_name: "Behance",
          images: {
            "32":
              "http://behance.vo.llnwd.net/profiles25/511814/32xe68d9ad064e2df842a010331a04f253f.jpg",
            "50":
              "http://behance.vo.llnwd.net/profiles25/511814/50xe68d9ad064e2df842a010331a04f253f.jpg",
            "78":
              "http://behance.vo.llnwd.net/profiles25/511814/78xe68d9ad064e2df842a010331a04f253f.jpg",
            "115":
              "http://behance.vo.llnwd.net/profiles25/511814/115xe68d9ad064e2df842a010331a04f253f.jpg",
            "129":
              "http://behance.vo.llnwd.net/profiles25/511814/129xe68d9ad064e2df842a010331a04f253f.jpg",
            "138":
              "http://behance.vo.llnwd.net/profiles25/511814/e68d9ad064e2df842a010331a04f253f.jpg"
          },
          fields: ["Design", "Interaction Design", "Web Development"]
        }
      },
      stats: {
        views: 8587,
        appreciations: 506,
        comments: 26
      }
    }
  ]
};

export const SAMPLE_USER_STATS = {
  stats: {
    today: {
      project_views: 220,
      project_appreciations: 5,
      project_comments: 0,
      profile_views: 28
    },
    all_time: {
      project_views: 33488,
      project_appreciations: 1841,
      project_comments: 20,
      profile_views: 6587
    }
  }
};

export const SAMPLE_USER_WORK_EXPERIENCE = {
  work_experience: [
    {
      position: "Co-Founder & Chief of Design",
      organization: "Behance",
      location: "Brooklyn, NY, USA"
    },
    {
      position: "Senior Designer",
      organization: "AR Media",
      location: "Brooklyn, NY, USA"
    },
    {
      position: "Senior Designer",
      organization: "Pro Am",
      location: "New York, NY, USA"
    },
    {
      position: "Junior Designer",
      organization: "Zeligmedia",
      location: "Barcelona, Spain"
    },
    {
      position: "Junior Designer",
      organization: "Triada ComunicaciÃ³n",
      location: "Barcelona, Spain"
    }
  ]
};

export const SAMPLE_USER_FOLLOWING = {
  following: [
    {
      id: 288550,
      first_name: "Benjamin",
      last_name: "Gage",
      username: "benjamingage",
      city: "Rochester",
      state: "New York",
      country: "United States",
      company: "",
      occupation: "",
      created_on: 1294252440,
      url: "http://www.behance.net/benjamingage",
      display_name: "Benjamin Gage",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles17/288550/32xe8e414c35f3df4f218aed7254a8d9fad.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles17/288550/50xe8e414c35f3df4f218aed7254a8d9fad.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles17/288550/78xe8e414c35f3df4f218aed7254a8d9fad.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles17/288550/115xe8e414c35f3df4f218aed7254a8d9fad.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles17/288550/129xe8e414c35f3df4f218aed7254a8d9fad.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles17/288550/e8e414c35f3df4f218aed7254a8d9fad.jpg"
      },
      fields: ["Graphic Design", "Illustration"]
    },
    {
      id: 383775,
      first_name: "Kristin",
      last_name: "Duhaime",
      username: "kristinduhaime",
      city: "Rochester",
      state: "New York",
      country: "United States",
      company: "",
      occupation: "",
      created_on: 1301339107,
      url: "http://www.behance.net/kristinduhaime",
      display_name: "Kristin Duhaime",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles19/383775/32xda2c2a7c840beefc016b0ca457408876.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles19/383775/50xda2c2a7c840beefc016b0ca457408876.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles19/383775/78xda2c2a7c840beefc016b0ca457408876.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles19/383775/115xda2c2a7c840beefc016b0ca457408876.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles19/383775/129xda2c2a7c840beefc016b0ca457408876.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles19/383775/da2c2a7c840beefc016b0ca457408876.jpg"
      },
      fields: ["Graphic Design"]
    },
    {
      id: 495335,
      first_name: "Florian",
      last_name: "Schulz",
      username: "getflourish",
      city: "Berlin",
      state: "",
      country: "Germany",
      company: "",
      occupation: "Interaction Design Student",
      created_on: 1307960997,
      url: "http://www.behance.net/getflourish",
      display_name: "Florian Schulz",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles22/495335/32x6fa4740750791e0e5e6b5b38a9be7641.png",
        "50":
          "http://behance.vo.llnwd.net/profiles22/495335/50x6fa4740750791e0e5e6b5b38a9be7641.png",
        "78":
          "http://behance.vo.llnwd.net/profiles22/495335/78x6fa4740750791e0e5e6b5b38a9be7641.png",
        "115":
          "http://behance.vo.llnwd.net/profiles22/495335/115x6fa4740750791e0e5e6b5b38a9be7641.png",
        "129":
          "http://behance.vo.llnwd.net/profiles22/495335/129x6fa4740750791e0e5e6b5b38a9be7641.png",
        "138":
          "http://behance.vo.llnwd.net/profiles22/495335/6fa4740750791e0e5e6b5b38a9be7641.png"
      },
      fields: ["User Interface Design", "Interaction Design", "Motion Graphics"]
    },
    {
      id: 1757145,
      first_name: "Amy",
      last_name: "Moore",
      username: "AmyMoorePhotography",
      city: "Brooklyn",
      state: "New York",
      country: "United States",
      company: "",
      occupation: "First Assistant",
      created_on: 1351220521,
      url: "http://www.behance.net/AmyMoorePhotography",
      display_name: "Amy Moore",
      images: {
        "32": "http://assets.behance.net/img/profile/no-image-32.jpg",
        "50": "http://assets.behance.net/img/profile/no-image-50.jpg",
        "78": "http://assets.behance.net/img/profile/no-image-78.jpg",
        "115": "http://assets.behance.net/img/profile/no-image-138.jpg",
        "129": "http://assets.behance.net/img/profile/no-image-138.jpg",
        "138": "http://assets.behance.net/img/profile/no-image-138.jpg"
      },
      fields: ["Creative Direction", "Photography", "Retouching"]
    },
    {
      id: 291380,
      first_name: "ClÃ©ment",
      last_name: "Faydi",
      username: "cfaydi",
      city: "New York",
      state: "New York",
      country: "United States",
      company: "Behance",
      occupation: "Lead Designer",
      created_on: 1294682580,
      url: "http://www.behance.net/cfaydi",
      display_name: "ClÃ©ment Faydi",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles14/291380/32x5225301d96f495564fbb4eb328d25625.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles14/291380/50x5225301d96f495564fbb4eb328d25625.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles14/291380/78x5225301d96f495564fbb4eb328d25625.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles14/291380/115x5225301d96f495564fbb4eb328d25625.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles14/291380/129x5225301d96f495564fbb4eb328d25625.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles14/291380/5225301d96f495564fbb4eb328d25625.jpg"
      },
      fields: ["Interaction Design", "User Interface Design", "Web Design"]
    },
    {
      id: 371633,
      first_name: "Raewyn",
      last_name: "Brandon",
      username: "raewynbrandon",
      city: "New York",
      state: "New York",
      country: "United States",
      company: "",
      occupation: "Web & Graphic Designer",
      created_on: 1300570825,
      url: "http://www.behance.net/raewynbrandon",
      display_name: "Raewyn Brandon",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles10/371633/32x4cadd7e2b4340429ffc29736fcc417bc.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles10/371633/50x4cadd7e2b4340429ffc29736fcc417bc.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles10/371633/78x4cadd7e2b4340429ffc29736fcc417bc.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles10/371633/115x4cadd7e2b4340429ffc29736fcc417bc.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles10/371633/129x4cadd7e2b4340429ffc29736fcc417bc.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles10/371633/4cadd7e2b4340429ffc29736fcc417bc.jpg"
      },
      fields: ["Graphic Design", "Print Design", "Web Design"]
    },
    {
      id: 93628,
      first_name: "Oscar",
      last_name: "Ramos Orozco",
      username: "oscarramosorozco",
      city: "New York",
      state: "New York",
      country: "United States",
      company: "Behance",
      occupation: "Chief Curator",
      created_on: 1233681049,
      url: "http://www.behance.net/oscarramosorozco",
      display_name: "Oscar Ramos Orozco",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles2/93628/32x163d8fd014195902b7ea1eba5f836dea.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles2/93628/50x163d8fd014195902b7ea1eba5f836dea.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles2/93628/78x163d8fd014195902b7ea1eba5f836dea.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles2/93628/115x163d8fd014195902b7ea1eba5f836dea.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles2/93628/129x163d8fd014195902b7ea1eba5f836dea.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles2/93628/163d8fd014195902b7ea1eba5f836dea.jpg"
      },
      fields: ["Photography", "Graphic Design", "Illustration"]
    },
    {
      id: 105936,
      first_name: "Samir",
      last_name: "Sadikhov",
      username: "samirsadixov",
      city: "Gandja",
      state: "",
      country: "Azerbaijan",
      company: "",
      occupation: "",
      created_on: 1243344816,
      url: "http://www.behance.net/samirsadixov",
      display_name: "Samir Sadikhov",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles5/105936/32x01059361243344889.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles5/105936/50x01059361243344889.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles5/105936/78x01059361243344889.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles5/105936/115x01059361243344889.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles5/105936/129x01059361243344889.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles5/105936/01059361243344889.jpg"
      },
      fields: ["Automotive Design", "Graphic Design", "Industrial Design"]
    },
    {
      id: 50001,
      first_name: "Matias",
      last_name: "Corea",
      username: "MatiasCorea",
      city: "Brooklyn",
      state: "New York",
      country: "United States",
      company: "Behance",
      occupation: "Co-Founder & Chief Designer",
      created_on: 1182475806,
      url: "http://www.behance.net/MatiasCorea",
      display_name: "Matias Corea",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles/50001/32x72f4b87f99ab4c477735576a0895b2b2.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles/50001/50x72f4b87f99ab4c477735576a0895b2b2.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles/50001/78x72f4b87f99ab4c477735576a0895b2b2.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles/50001/115x72f4b87f99ab4c477735576a0895b2b2.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles/50001/129x72f4b87f99ab4c477735576a0895b2b2.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles/50001/72f4b87f99ab4c477735576a0895b2b2.jpg"
      },
      fields: ["Interaction Design", "Typography"]
    },
    {
      id: 70905,
      first_name: "Zach",
      last_name: "McCullough",
      username: "zachmccullough",
      city: "New York",
      state: "New York",
      country: "United States",
      company: "Behance LLC",
      occupation: "Senior Designer",
      created_on: 1207321164,
      url: "http://www.behance.net/zachmccullough",
      display_name: "Zach McCullough",
      images: {
        "32":
          "http://behance.vo.llnwd.net/profiles/70905/32xaf7eedb53744bc0915131f5eb9e77c17.jpg",
        "50":
          "http://behance.vo.llnwd.net/profiles/70905/50xaf7eedb53744bc0915131f5eb9e77c17.jpg",
        "78":
          "http://behance.vo.llnwd.net/profiles/70905/78xaf7eedb53744bc0915131f5eb9e77c17.jpg",
        "115":
          "http://behance.vo.llnwd.net/profiles/70905/115xaf7eedb53744bc0915131f5eb9e77c17.jpg",
        "129":
          "http://behance.vo.llnwd.net/profiles/70905/129xaf7eedb53744bc0915131f5eb9e77c17.jpg",
        "138":
          "http://behance.vo.llnwd.net/profiles/70905/af7eedb53744bc0915131f5eb9e77c17.jpg"
      },
      fields: ["Interaction Design", "User Interface Design", "Web Design"]
    }
  ]
};
