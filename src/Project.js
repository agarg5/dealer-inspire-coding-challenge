import "./UserPage.css";
import { getFormattedDate } from "./utils";
import { Typography, Card, Button, ListItemText } from "@material-ui/core/";
import React, { useState } from "react";
import PropTypes from "prop-types";
import UserBasicInfo from "./UserBasicInfo";

const PROJECT_IMAGE_SIZE = 202;

const Owner = props => {
  const [hovering, setHovering] = useState(false);

  const owner = props.ownerInfo;
  if (!owner) return null;
  const { display_name, url } = owner;

  return (
    <ListItemText>
      <Typography
        className={"owner-hover"}
        onMouseEnter={() => setHovering(true)}
        onMouseLeave={() => setHovering(false)}
      >
        {url ? <a href={url}>{display_name}</a> : display_name}
      </Typography>
      {hovering && <UserBasicInfo user={owner} />}
    </ListItemText>
  );
};

function Project(props) {
  const { project } = props;
  const {
    covers,
    name,
    url,
    created_on: createdOnTimestampSeconds,
    modified_on: modifiedOnTimestampSeconds,
    published_on: publishedOnTimestampSeconds,
    fields,
    owners,
    display_name,
    mature_content: matureContent,
    stats
  } = project;

  const { views, appreciations, comments } = stats;

  const nameDisplay = (
    <Typography gutterBottom variant="h5" component="h2">
      {name}
    </Typography>
  );
  const fieldsDislay = fields && <Typography>{fields.join(" | ")}</Typography>;
  const modifiedOnDisplay = (
    <Typography>
      {modifiedOnTimestampSeconds &&
        `Modified: ${getFormattedDate(modifiedOnTimestampSeconds)}`}
    </Typography>
  );
  const publishedOnDisplay = (
    <Typography>
      {publishedOnTimestampSeconds &&
        `Published: ${getFormattedDate(publishedOnTimestampSeconds)}`}
    </Typography>
  );
  const createdOnDisplay = (
    <Typography>
      {createdOnTimestampSeconds &&
        `Created: ${getFormattedDate(createdOnTimestampSeconds)}`}
    </Typography>
  );

  const ownersDisplay = owners && (
    <>
      <Typography variant="h18">Owners:</Typography>
      <>
        {owners.map(owner => (
          <Owner ownerInfo={owner} key={owner.id} />
        ))}
      </>
    </>
  );

  const CoverDisplay = covers && (
    <img
      src={covers[PROJECT_IMAGE_SIZE]}
      alt={`${display_name || `project's image`}`}
    />
  );

  const matureContentDisplay = matureContent > 0 && (
    <Typography>Warning: Mature Content</Typography>
  );

  const viewsDisplay = views >= 0 && (
    <Typography>Project Views: {views}</Typography>
  );

  const commentsDisplay = comments >= 0 && (
    <Typography>Project Comments: {comments}</Typography>
  );

  const appreciationsDisplay = appreciations >= 0 && (
    <Typography>Project Appreciations: {appreciations}</Typography>
  );

  return (
    <Card className="project-card">
      {CoverDisplay}
      {nameDisplay}
      <Button size="small" color="primary" href={url}>
        Learn More
      </Button>
      {matureContentDisplay}
      {fieldsDislay}
      {createdOnDisplay}
      {modifiedOnDisplay}
      {publishedOnDisplay}
      {viewsDisplay}
      {appreciationsDisplay}
      {commentsDisplay}
      {ownersDisplay}
    </Card>
  );
}

export default Project;

Project.propTypes = {
  project: PropTypes.object
};
