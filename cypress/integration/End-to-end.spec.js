describe("Search Testing", () => {
  it("Search makes an API call for the users and gets the value", async () => {
    cy.visit("/");

    cy.get("input").type("matias corea");
    cy.wait(250);
    cy.contains("Corea");

    cy.visit("/user/matiascorea");

    cy.contains("Graphic Design");
    cy.contains("Co-Founder of Behance");

    cy.get(`.About`).click();
    cy.contains("Born and raised in Barcelona");

    cy.get(`.About`).click();
    cy.contains("Born and raised in Barcelona");

    cy.get(`.Links`).click();
    cy.contains("Desing Droplets Interview");

    cy.get(`.Projects`).click();
    cy.wait(500);
    cy.contains("The Behance Book of Creative Work");
  });
});
